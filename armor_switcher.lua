local pim = peripheral.wrap("front")

local armorSwitched = false

while true do
  sleep(0)
  
  if pim.getInventorySize() > 0 then
    if not armorSwitched then
      for i = 0, 3 do 
        local pushDirection = "NORTH"
        if pim.pushItem(pushDirection, 37 + i, 1, 4 - i) == 0 then
          pushDirection = "SOUTH"
          if pim.getInventorySize() > 0 then
            pim.pushItem(pushDirection, 37 + i, 1, 4 - i)
          end
        end
     
        local pullDirection = "SOUTH"
        if pushDirection == "SOUTH" then pullDirection = "NORTH" end
         
        if pim.pullItem(pullDirection, 4 - i, 1, 37 + i) == 0 then
          print("ERROR: Unable to pull armor.")
        end
        
        armorSwitched = true
      end
    end
  else
    armorSwitched = false
  end
end
