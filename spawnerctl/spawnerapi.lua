spawnerapi = {}

spawnerapi.loadSpawnerData = function()
	if not fs.exists(path .. "/spawnerdata") then
		print("Error: Spawner data not found")
		return
	end

	local f = fs.open(path .. "/spawnerdata", "r")
	local spawnerData = textutils.unserialise(f.readAll())
	f.close()

	return spawnerData
end

spawnerapi.saveSpawnerData = function(spawnerData)
	if not fs.exists(path .. "/spawnerdata") then
		print("Error: Spawner data not found")
		return
	end

	local f = fs.open(path .. "/spawnerdata", "w")
	f.write(textutils.serialise(spawnerData))
	f.flush()
	f.close()
end

spawnerapi.getState = function(spawnerId)
	rednet.send(tonumber(spawnerId), "get_state", "spawnerserv" .. spawnerId)

	local id, state = rednet.receive("spawnerserv" .. spawnerId, 1)
	if id == nil then
		return nil
	end

	return state == "true"
end

spawnerapi.list = function()
	local spawnerData = spawnerapi.loadSpawnerData()
	print("List of spawners available:")

	local sets = {}
	for name, id in spairs(spawnerData) do
		sets[#sets + 1] = {name, tostring(id), tostring(spawnerapi.getState(id))}
	end

	textutils.pagedTabulate(unpack(sets));
end

spawnerapi.checkParameters = function(args, argcRequired, numbersRequired)
	assert(#args >= argcRequired, "Wrong number of arguments")

	for _, value in pairs(numbersRequired) do
		assert(tonumber(args[value]) ~= nil, "Wrong argument type, required 'number' but got 'string'")
	end
end

spawnerapi.add = function(args)
	spawnerapi.checkParameters(args, 3, {3})

	local spawnerData = spawnerapi.loadSpawnerData()
	spawnerData[args[2]] = tonumber(args[3])

	if args[1] ~= "quiet" then
		print("Adding spawner with name '" .. args[2] .. "' and id " .. args[3])
	end

	spawnerapi.saveSpawnerData(spawnerData);
end

spawnerapi.remove = function(args)
	spawnerapi.checkParameters(args, 2, {})

	local spawnerData = spawnerapi.loadSpawnerData()
	spawnerData[args[2]] = nil

	print("Removing spawner with name '" .. args[2] .. "'")

	spawnerapi.saveSpawnerData(spawnerData);
end

spawnerapi.edit = function(args)
	spawnerapi.checkParameters(args, 3, {3})

	local spawnerData = spawnerapi.loadSpawnerData()
	spawnerData[args[2]] = tonumber(args[3])

	if args[1] ~= "quiet" then
		print("Changing spawner '" .. args[2] .. "' id to " .. args[3])
	end

	spawnerapi.saveSpawnerData(spawnerData);
end

spawnerapi.enable = function(args)
	spawnerapi.checkParameters(args, 2, {})

	local spawnerData = spawnerapi.loadSpawnerData()
	rednet.send(spawnerData[args[2]], "set_state true", "spawnerserv" .. spawnerData[args[2]])

	if args[1] ~= "quiet" then
		print("Spawner '" .. args[2]  .. "' enabled")
	end
end

spawnerapi.disable = function(args)
	spawnerapi.checkParameters(args, 2, {})

	local spawnerData = spawnerapi.loadSpawnerData()
	rednet.send(spawnerData[args[2]], "set_state false", "spawnerserv" .. spawnerData[args[2]])

	if args[1] ~= "quiet" then
		print("Spawner '" .. args[2]  .. "' disabled")
	end
end

