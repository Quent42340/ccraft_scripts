checkParameters = function(args, argcRequired, numbersRequired)
	if #args < argcRequired then
		print("Error: Wrong number of arguments");
		return false
	end

	for key, value in pairs(numbersRequired) do
		if tonumber(args[value]) == nil then
			print("Error: Wrong argument type, required 'number' but got 'string'")
			return false
		end
	end

	return true
end

add = function(self, args)
	if not checkParameters(args, 3, {3}) then
		return
	end

	local spawnerData = loadSpawnerData()
	spawnerData[args[2]] = {tonumber(args[3]), false}

	print("Adding spawner with name '" .. args[2] .. "' and id " .. args[3])

	saveSpawnerData(spawnerData);
end

remove = function(self, args)
	if not checkParameters(args, 3, {3}) then
		return
	end

	local spawnerData = loadSpawnerData()
	spawnerData[args[2]] = nil

	print("Removing spawner with name '" .. args[2] .. "' and id " .. args[3])

	saveSpawnerData(spawnerData);
end

edit = function(self, args)
	if not checkParameters(args, 3, {3}) then
		return
	end

	local spawnerData = loadSpawnerData()
	spawnerData[argv[2]][1] = tonumber(args[3])

	print("Changing spawner '" .. args[2] .. "' id to " .. args[3])

	saveSpawnerData(spawnerData);
end

enable = function(self, args)
	if not checkParameters(args, 2, {}) then
		return
	end

	local spawnerData = loadSpawnerData()
	spawnerData[args[2]][2] = true
	saveSpawnerData(spawnerData);

	rednet.open("back")
	rednet.send(spawnerData[args[2]][1], "true")
	rednet.close("back")

	print("Spawner '" .. args[2]  .. "' enabled")
end

disable = function(self, args)
	if not checkParameters(args, 2, {}) then
		return
	end

	local spawnerData = loadSpawnerData()
	spawnerData[args[2]][2] = false
	saveSpawnerData(spawnerData);

	rednet.open("back")
	rednet.send(spawnerData[args[2]][1], "false")
	rednet.close("back")

	print("Spawner '" .. args[2]  .. "' disabled")
end

