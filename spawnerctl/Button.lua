Button = {
	new = function(x, y, text, callback)
		local width = #text + 6
		local height = 3

		local draw = function(self)
			if self.isClicked == nil then
				term.setBackgroundColour(colours.red)
			elseif not self.isClicked then
				term.setBackgroundColour(colours.grey)
			else
				term.setBackgroundColour(colours.green)
			end

			-- if self.timeRequestSent ~= nil then
				for y = self.y, self.y + self.height - 1 do
					term.setCursorPos(self.x, y)
					term.write(string.rep(" ", self.width))
				end

				term.setCursorPos(self.x + self.width / 2 - #self.text / 2,
								  self.y + self.height / 2)

				term.write(self.temporaryText or self.text)
				-- term.write(self.text)

				term.setBackgroundColour(colours.black)
			-- end
		end

		local updateState = function(self, state)
			self.isClicked = state
		end

		local setTemporaryText = function(self, text)
			self.temporaryText = text
		end

		local clearTemporaryText = function(self)
			self.temporaryText = nil
		end

		local readMouse = function(self, mouseX, mouseY)
			if mouseX >= self.x and mouseX < self.x + self.width and
			   mouseY >= self.y and mouseY < self.y + self.height then
				if self.callback ~= nil then
					self:callback()
				end

				if self.isClicked ~= nil then
					self.isClicked = not self.isClicked
				end
			end
		end

		return {
			-- Variables
			x = x,
			y = y,
			width = width,
			height = height,
			text = text,
			temporaryText = nil,
			isClicked = false,
			callback = callback,

			-- Functions
			draw = draw,
			updateState = updateState,
			setTemporaryText = setTemporaryText,
			clearTemporaryText = clearTemporaryText,
			readMouse = readMouse
		}
	end
}

