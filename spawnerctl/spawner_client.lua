arg = {...}

spawners = {
    Enderman   = 17,
    PinkSlime  = 16,
    Blizz      = 15,
    Blaze      = 13,
    Blitz      = 14
}

if #arg < 2 then
    print("Usage: spawner_client <spawner_name> <boolean>")
elseif spawners[arg[1]] == nil then
	print("Error: Unknown spawner '" .. tostring(arg[1]) .. "'");
else
    rednet.open("left")

    rednet.send(spawners[arg[1]], arg[2])

    print("Spawner '" .. arg[1] .. "' set to state '" .. arg[2] .."'")

    rednet.close("left")
end
