-- TODO: Make these global computer settings
local versionString = "0.1.7a"
local path = "/spawnerctl"

dofile(path .. "/SpawnerController")

local args = {...}

local spawnerCtl = SpawnerController.new()

if #args >= 1 then
	if spawnerCtl[args[1]] == nil then
		print("Error: Unknown command. Use 'spawnerctl help' to list available commands.")
		return
	else
		spawnerCtl[args[1]](spawnerCtl, args)
	end
else
	term.clear()
	term.setCursorPos(1, 1)
	print("/-----------------------------------------------\\")
	print("|                                               |")
	print("|           Spawner Controller " .. versionString .. "           |")
	print("|    Type 'help' to list available commands.    |")
	print("|                                               |")
	print("\\-----------------------------------------------/")

	local args = {""}
	while args[1] ~= "exit" do
		dofile(path .. "/string_utils")
		write("\nspawnerctl> ")
		args = string.split(read(), " ")

		if spawnerCtl[args[1]] ~= nil then
			spawnerCtl[args[1]](spawnerCtl, args)
		elseif args[1] ~= "exit" then
			printError("Unknown command. Type 'help' to list available commands.");
		end
	end

	term.clear()
	term.setCursorPos(1, 1)
end

