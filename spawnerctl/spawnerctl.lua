-- TODO: Make these global computer settings
versionString = "0.4.0b"
path = "/spawnerctl"

dofile(path .. "/utils")
dofile(path .. "/SpawnerController")

local args = {...}

local spawnerCtl = SpawnerController.new()
local isModemPresent = peripheral.getType("back") == "modem"

if isModemPresent then
	rednet.open("back")
end

if #args >= 1 then
	if spawnerCtl[args[1]] == nil then
		print("Error: Unknown command. Use 'spawnerctl help' to list available commands.")
		return
	else
		spawnerCtl[args[1]](args)
	end
else
	term.clear()
	term.setCursorPos(1, 1)

	local w, h = term.getSize()
	local longName, shortName = "Spawner Controller", "spawnerctl"
	local longHelp, shortHelp = "Type 'help' to list available commands.", "For help, type 'help'."
	if w < #longHelp + 6 or h < 7 then
		print(shortName .. " " .. versionString)
		print("For help, type 'help'.")
	else
		print("/" .. string.rep("-", w - 2) .. "\\")
		print("|" .. string.rep(" ", w - 2) .. "|")

		local nameSize = #longName + #versionString + 1
		local spaces = string.rep(" ", (w - nameSize - 2) / 2)
		print("|" .. spaces .. longName .. " " .. versionString .. spaces .. "|")

		spaces = string.rep(" ", (w - #longHelp - 2) / 2)
		print("|" .. spaces .. longHelp .. spaces .. "|")

		print("|" .. string.rep(" ", w - 2) .. "|")
		print("\\" .. string.rep("-", w - 2) .. "/")
	end

	argv = {""}
	while argv[1] ~= "exit" do
		write("\nspawnerctl> ")
		argv = string.split(read(), " ")

		if spawnerCtl[argv[1]] ~= nil then
			spawnerCtl[argv[1]](argv)
		elseif args[1] ~= "exit" then
			printError("Unknown command. Type 'help' to list available commands.");
		end
	end

	term.clear()
	term.setCursorPos(1, 1)
end

if isModemPresent then
	rednet.close("back")
end

