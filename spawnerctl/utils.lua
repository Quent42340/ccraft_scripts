function string:split(sSeparator, nMax, bRegexp)
   assert(sSeparator ~= '')
   assert(nMax == nil or nMax >= 1)

   local aRecord = {}

   if self:len() > 0 then
      local bPlain = not bRegexp
      nMax = nMax or -1

      local nField, nStart = 1, 1
      local nFirst,nLast = self:find(sSeparator, nStart, bPlain)
      while nFirst and nMax ~= 0 do
         aRecord[nField] = self:sub(nStart, nFirst-1)
         nField = nField+1
         nStart = nLast+1
         nFirst,nLast = self:find(sSeparator, nStart, bPlain)
         nMax = nMax-1
      end
      aRecord[nField] = self:sub(nStart)
   end

   return aRecord
end

function spairs(t, order)
    -- collect the keys
    local keys = {}
    for k in pairs(t) do keys[#keys+1] = k end

    -- if order function given, sort by it by passing the table and keys a, b,
    -- otherwise just sort the keys
    if order then
        table.sort(keys, function(a,b) return order(t, a, b) end)
    else
        table.sort(keys)
    end

    -- return the iterator function
    local i = 0
    return function()
        i = i + 1
        if keys[i] then
            return keys[i], t[keys[i]]
        end
    end
end

-- Little fix
--rednet.receive = function( sProtocolFilter, nTimeout )
--	-- The parameters used to be ( nTimeout ), detect this case for backwards compatibility
--	if type(sProtocolFilter) == "number" and nTimeout == nil then
--		sProtocolFilter, nTimeout = nil, sProtocolFilter
--	end
--
--	-- Start the timer
--	local timer = nil
--	local sFilter = nil
--	if nTimeout then
--		timer = os.startTimer( nTimeout )
--		sFilter = nil
--	else
--		sFilter = "rednet_message"
--	end
--
--	-- Wait for events
--	while true do
--		local sEvent, p1, p2, p3 = os.pullEvent( sFilter )
--		if sEvent == "rednet_message" then
--			-- Return the first matching rednet_message
--			local nSenderID, message, sProtocol = p1, p2, p3
--			if sProtocolFilter == nil or sProtocol == sProtocolFilter then
--				return nSenderID, message, sProtocol
--			else
--				os.queueEvent(sEvent, p1, p2, p3)
--			end
--		elseif sEvent == "timer" then
--			-- Return nil if we timeout
--			if p1 == timer then
--				return nil
--			else
--				os.queueEvent(sEvent, p1, p2, p3)
--			end
--		end
--	end
--end
