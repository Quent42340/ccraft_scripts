rednet.open("front")

print("Hello. I'm a spawner. My id is " .. tostring(os.getComputerID()) .. ".")

while 1 do
  local id, message = rednet.receive()
  if message == "true" then
    redstone.setOutput("back", true)
  elseif message == "false" then
    redstone.setOutput("back", false)
  end

  if (message == "true") or (message == "false") then
    print("State set to '" .. message .. "' by computer " .. tostring(id))
  end
end

rednet.close("front")
