SpawnerController = {
	new = function()
		local printVersion = function()
			print("spawnerctl " .. versionString)
		end

		local printUsage = function()
			print("Usage: spawnerctl [<command> [args...]]")
			print("An interactive prompt will run if no command is provided.")
		end

		local printHelp = function()
			print("Commands:")
			print("  version              Print version")
			print("  usage                Print usage")
			print("  help                 Print help")
			print("  clear                Clear screen")
			print("  make_disk            Make an installation disk")
			print("  install              Install spawner manager")
			print("  update               Update spawner manager")
			print("  list                 List available spawners")
			print("  add <spawner> <id>   Add new spawner")
			print("  remove <spawner>     Remove spawner")
			print("  edit <spawner> <id>  Change spawner id")
			print("  enable <spawner>     Enable spawner")
			print("  disable <spawner>    Disable spawner")
			print("  app [--log]          SpawnerApp")
			print("  server <name>        Run spawner server")
			print("  glasses              Run glasses server")
		end

		dofile(path .. "/spawnerapi")
		dofile(path .. "/spawnerapp")
		dofile(path .. "/spawnersys")

		dofile(path .. "/spawnerserv")
		dofile(path .. "/glasses")

		return {
			version   = printVersion,
			usage     = printUsage,
			help      = printHelp,
			clear     = spawnersys.clear,
			make_disk = spawnersys.makeInstallationDisk,
			install   = spawnersys.install,
			update    = spawnersys.update,
			list      = spawnerapi.list,
			add       = spawnerapi.add,
			remove    = spawnerapi.remove,
			edit      = spawnerapi.edit,
			enable    = spawnerapi.enable,
			disable   = spawnerapi.disable,
			app       = spawnerapp.run,
			server    = spawnerserv,
			glasses   = spawner_glasses
		}
	end
}

