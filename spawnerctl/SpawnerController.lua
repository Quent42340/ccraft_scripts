local versionString = "0.1.7a"
local path = "/spawnerctl"

SpawnerController = {
	new = function()
		local version = function()
			print("spawnerctl " .. versionString)
		end

		local usage = function()
			print("Usage: spawnerctl [<command> [args...]]")
			print("An interactive prompt will run if no command is provided.")
		end

		local help = function()
			print("Commands:")
			print("  version              Print version")
			print("  usage                Print usage")
			print("  help                 Print help")
			print("  clear                Clear screen")
			print("  make_disk            Make an installation disk")
			print("  install              Install spawner manager")
			print("  list                 List available spawners")
			print("  add <spawner> <id>   Add new spawner")
			print("  remove <spawner>     Remove spawner")
			print("  edit <spawner> <id>  Change spawner id")
			print("  enable <spawner>     Enable spawner")
			print("  disable <spawner>    Disable spawner")
		end

		local clear = function(self)
			term.clear()
			term.setCursorPos(1, 1)
		end

		local make_disk = function()
			if not fs.exists("/disk") then
				print("Error: No disk available in '/disk'")
				return
			end

			print("Creating installation disk...");

			if fs.exists("/disk/install") then
				fs.delete("/disk/install")
			end

			if fs.exists("/disk/spawnerdata") then
				fs.delete("/disk/spawnerdata")
			end

			fs.copy(path .. "/install", "/disk/install")
			fs.copy(path .. "/spawnerdata", "/disk/spawnerdata")

			local f = fs.open("/disk/startup", "w")
			f.write("shell.run(\"/disk/install\")")
			f.close()

			shell.run("label set right \"spawnerctl " .. versionString .. " Installer\"");
		end

		local install = function()
			print("Installing spawner manager...")

			if fs.exists("/disk/spawnerctl") then
				if fs.exists(path .. "/spawnerctl") then
					fs.delete(path .. "/spawnerctl")
				end

				fs.copy("/disk/spawnerctl", path .. "/spawnerctl")
			end

			if fs.exists("/disk/spawnerdata") then
				if fs.exists(path .. "/spawnerdata") then
					fs.delete(path .. "/spawnerdata")
				end

				fs.copy("/disk/spawnerdata", path .. "/spawnerdata")
			elseif not fs.exists(path .. "/spawnerdata") then
				local f = fs.open(path .. "/spawnerdata", "w")
				f.write("{}")
				f.close()
			end

			-- TODO: Do a global startup file which will startup every folder except disks and rom
			f = fs.open("/startup", "w")
			f.write("local monitorTab = multishell.launch({shell = shell}, \"/rom/programs/monitor\", \"top\", \"" .. path .. "/spawnerctl\")\n")
			f.write("multishell.setTitle(monitorTab, \"monitor\")\n")
			f.write("local tab = multishell.launch({shell = shell}, \"" .. path ..  "/spawnerctl\")\n")
			f.write("multishell.setTitle(tab, \"spawnerctl\")")
			f.close()

			print("Successfully installed! Please remove disk if any.")
			-- TODO: Don't let the computer reboot if a disk is still present
			print("Press Enter to reboot.")

			read()
			os.reboot();
		end

		local loadSpawnerData = function()
			if not fs.exists(path .. "/spawnerdata") then
				print("Error: Spawner data not found")
				return
			end

			local f = fs.open(path .. "/spawnerdata", "r")
			local spawnerData = textutils.unserialise(f.readAll())
			f.close()

			return spawnerData
		end

		local saveSpawnerData = function(spawnerData)
			if not fs.exists(path .. "/spawnerdata") then
				print("Error: Spawner data not found")
				return
			end

			local f = fs.open(path .. "/spawnerdata", "w")
			f.write(textutils.serialise(spawnerData))
			f.flush()
			f.close()
		end

		local list = function(self)
			local spawnerData = loadSpawnerData()
			print("List of spawners available:")

			local sets = {}
			for key, value in pairs(spawnerData) do
				sets[#sets + 1] = {key, tostring(value[1]), tostring(value[2])}
			end

			textutils.pagedTabulate(unpack(sets));
		end

		dofile(path .. "/spawnerapi")

		return {
			versionString = versionString,
			version = version,
			usage = usage,
			help = help,
			clear = clear,
			make_disk = make_disk,
			install = install,
			list = list,
			add = add,
			remove = remove,
			edit = edit,
			enable = enable,
			disable = disable,
		}
	end
}

