spawnerapp = {}

spawnerapp.mouseClickEvent = function(x, y)
	if x == 1 and y == 1 then
		spawnerapp.isRunning = false
	end

	for _, button in pairs(spawnerapp.buttons) do
		button:readMouse(x, y)
	end
end

spawnerapp.timerEvent = function(timer)
	if timer == spawnerapp.timer or timer == spawnerapp.timeoutCheckTimer then
		for _, button in pairs(spawnerapp.buttons) do
			local now = os.clock()
			if button.timeRequestSent and now - button.timeRequestSent > 5 then
				button:updateState(nil)
				button:setTemporaryText("Timeout: " .. tostring(now - button.timeRequestSent))
				print("[" .. tostring(os.clock()) .. "] Button '" .. button.text .. "' state timeout: " .. tostring(now - button.timeRequestSent))
			end
		end

		spawnerapp.timeoutCheckTimer = os.startTimer(2)
	end

	if timer == spawnerapp.timer or timer == spawnerapp.stateTimer then
		for _, button in pairs(spawnerapp.buttons) do
			if button.timeRequestSent == nil then
				local id = tonumber(spawnerapp.spawnerData[button.text])
				rednet.send(id, "get_state", "spawnerserv" .. id)
				button.timeRequestSent = now
			end
		end

		spawnerapp.stateTimer = os.startTimer(1)
	end
end

spawnerapp.rednetMessageEvent = function(p2, p3)
	if p3 == "spawnerinfo" then
		local args = string.split(p2, " ")
		assert(args[2] and tonumber(args[3]), "Bad command: '" .. p2 .. "'")
		args[3] = tonumber(args[3])

		-- TODO: Don't use add and edit
		if spawnerapp.spawnerData[args[2]] then
			if spawnerapp.spawnerData[args[2]] ~= args[3] then
				spawnerapi.edit({"quiet", args[2], args[3]})
			end
		else
			spawnerapi.add({"quiet", args[2], args[3]})
			-- TODO: Create a class ButtonList which whill handle x and y for me
			spawnerapp.buttons[#spawnerapp.buttons + 1] = Button.new(spawnerapp.x, spawnerapp.y, args[2], buttonCallback)
			spawnerapp.y = spawnerapp.y + 4
		end

		spawnerapp.spawnerData = spawnerapi.loadSpawnerData()
	elseif p3 and string.sub(p3, 0, 11) == "spawnerserv" then
		for _, button in pairs(spawnerapp.buttons) do
			if spawnerapp.spawnerData[button.text] == p1 then
				button:updateState(p2 == "true")
				button.timeRequestSent = nil
				print("[" .. tostring(os.clock()) .. "] Button '" .. button.text .. "' state updated: " .. tostring(p2 == "true"))
			end
		end
	end
end

spawnerapp.processEvent = function(event, p1, p2, p3)
	if event == "mouse_click" then
		spawnerapp.mouseClickEvent(p2, p3)
	elseif event == "timer" then
		spawnerapp.timerEvent(p1)
	elseif event == "rednet_message" then
		spawnerapp.rednetMessageEvent(p2, p3)
	else
		-- ??????
	end
end

spawnerapp.initButtons = function()
	dofile(path .. "/Button")

	local w, h = term.getSize()
	spawnerapp.x, spawnerapp.y = 2, 3

	-- TODO: Make exit cross a Button too
	local widestButtonInColumn = 0
	local buttonCallback = function(self)
		if self.isClicked == nil then
			return
		elseif not self.isClicked then
			spawnerapi.enable({"quiet", self.text})
		else
			spawnerapi.disable({"quiet", self.text})
		end
	end

	spawnerapp.buttons = {}
	for name, id in spairs(spawnerapp.spawnerData) do
		spawnerapp.buttons[#spawnerapp.buttons + 1] = Button.new(spawnerapp.x, spawnerapp.y, name, buttonCallback)
		spawnerapp.y = spawnerapp.y + 4

		local button = spawnerapp.buttons[#spawnerapp.buttons]
		widestButtonInColumn = math.max(widestButtonInColumn, button.width)

		if spawnerapp.y > h - button.height then
			spawnerapp.y = 3
			spawnerapp.x = spawnerapp.x + widestButtonInColumn + 1
		end
	end
end

spawnerapp.mainLoop = function(optionLog)
	spawnerapp.isRunning = true
	spawnerapp.timer = os.startTimer(1)

	if optionLog and optionLog == "--log" then
		spawnerapp.eventLog = fs.open("/spawnerctl/eventlog", "w")
	end

	-- local rebootTime = os.clock() + 300

	while spawnerapp.isRunning == true do
		local event, p1, p2, p3 = os.pullEvent()
		spawnerapp.processEvent(event, p1, p2, p3)

		spawnerapp.draw()

		-- if os.clock() > rebootTime then
		-- 	shell.run("reboot")
		-- end

		if optionLog and optionLog == "--log" then
			spawnerapp.eventLog.write("[" .. tostring(os.clock()) .. "] Event received: '" .. tostring(event) .. "' '" .. tostring(p1) .. "' '" .. tostring(p2) .. "' '" .. tostring(p3) .. "\n")
			spawnerapp.eventLog.flush()
		end
	end

	if optionLog and optionLog == "--log" then
		spawnerapp.eventLog.close()
	end
end

spawnerapp.run = function(optionLog)
	term.clear()

	spawnerapp.spawnerData = spawnerapi.loadSpawnerData()
	spawnerapp.initButtons()

	spawnerapp.mainLoop(optionLog)

	sleep(0.5)
	term.setCursorPos(1, 1)
	term.setBackgroundColour(colours.black)
	term.setTextColour(colours.white)
	term.clear()
end

spawnerapp.draw = function()
	local w, h = term.getSize()
	local title = "spawnerctl " .. versionString
	term.setCursorPos(w / 2 - #title / 2 + 2, 1)
	term.setBackgroundColour(colours.blue)
	term.setTextColour(colours.white)
	term.write(title)

	term.setCursorPos(1, 1)
	if spawnerapp.isRunning then
		term.setBackgroundColour(colours.grey)
	else
		term.setBackgroundColour(colours.black)
	end
	term.setTextColour(colours.black)
	term.write("X");
	term.setBackgroundColour(colours.black)
	term.setTextColour(colours.white)

	for _, button in pairs(spawnerapp.buttons) do
		button:draw()
	end
end

