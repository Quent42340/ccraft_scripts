spawnerserv = function(args)
	if #args > 1 then
		os.setComputerLabel(args[2])
	else
		args[2] = os.getComputerLabel()
	end

	local myId = os.getComputerID()
	local protocol = "spawnerserv" .. myId

	print("Server started.")
	print("Name:        " .. args[2])
	print("Computer ID: " .. tostring(myId))

	rednet.open("front")
	rednet.broadcast("spawnerinfo " .. args[2] .. " " .. myId, "spawnerinfo")
	rednet.close("front")

	redstone.setOutput("back", true)

	-- local rebootTime = os.clock() + 300;

	while true do
		rednet.open("front")

		print("[" .. os.clock() .. "] Receiving message on '" .. protocol .. "'...")
		local id, message = rednet.receive(protocol)

		print("[" .. os.clock() .. "] Message received from id " .. id .. ": '" .. message .. "'")
		-- if message == "set_state true" or message == "set_state false" then
		-- 	print("* " .. id .. ": " .. message)
		-- end

		if message == "set_state true" then
			print("[" .. os.clock() .. "] Setting redstone output to false")
			redstone.setOutput("back", false)
		elseif message == "set_state false" then
			print("[" .. os.clock() .. "] Setting redstone output to true")
			redstone.setOutput("back", true)
		elseif message == "get_state" then
			print("[" .. os.clock() .. "] Returning spawner state -> " .. tostring(not redstone.getOutput("back")))
			rednet.send(id, tostring(not redstone.getOutput("back")), protocol)
			-- print("-> " .. id .. " " .. tostring(redstone.getOutput("back")))
		end

		rednet.close("front")

		-- if os.clock() > rebootTime then
		-- 	shell.run("reboot")
		-- end
	end
end

