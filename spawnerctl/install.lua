local path = "/spawnerctl"
local files = {"install", "spawnerctl", "SpawnerController", "spawnerapi", "string_utils"}

if path ~= "" and not fs.exists(path) then
	fs.makeDir(path)
end

for _, value in pairs(files) do
	write("Copying file '" .. value .. "'... ")

	local request = http.get("https://bitbucket.org/Quent42340/ccraft_scripts/raw/HEAD/spawnerctl/" .. value .. ".lua");
	-- TODO: Check errors
	local f = fs.open(path .. "/" .. value, "w")
	f.write(request.readAll())
	f.close()
	print("Done. (" .. request.getResponseCode() .. ")")
	request.close();
end

shell.run(path .. "/spawnerctl install")

